export default class Sample {
  private argd: string;
  constructor(argd: string) {
    this.argd = argd;
  }

  public getArg(): string {
    return this.argd;
  }
}
