import * as bodyParser from 'body-parser';
import express from 'express';
import http from 'http';

const app = express();
const server = http.createServer(app);
app.use(bodyParser.json());

app.post('/a', (req: express.Request, res: express.Response) => {
  const { first = 0, second = 0 } = req.body.numbers;
  const result: number = first + second;
  if (result > 9999999999) {
    res.status(413);
    res.end();
  } else {
    res.status(200);
    res.json(result);
  }
});

app.post('/b', (req: express.Request, res: express.Response) => {
  const { first = 0, second = 0 } = req.body.numbers;
  const result: number = first * second;
  if (result > 9999999999) {
    res.status(413);
    res.end();
  } else {
    res.status(200);
    res.json(result);
  }
});

app.post('/c', (req: express.Request, res: express.Response) => {
  const { first = 0, second = 0 } = req.body.numbers;
  const result: number = first - second;
  if (result > 9999999999) {
    res.status(413);
    res.end();
  } else {
    res.status(200);
    res.json(result);
  }
});

app.post('/d', (req: express.Request, res: express.Response) => {
  const { first = 1, second = 1 } = req.body.numbers;
  if (second === 0) {
    res.status(400);
    res.end();
  } else {
    const result: number = first / second;
    if (result > 9999999999) {
      res.status(413);
      res.end();
    } else {
      res.status(200);
      res.json(result);
    }
  }
});

app.get('/', (req: express.Request, res: express.Response) => {
  res.status(201);
  res.end();
});

export default app;
