import { expect } from 'chai';

import Sample from '../src/Sample';

describe('Test sample 1', () => {
  it('constructor', () => {
    const a = 'random';
    const s = new Sample(a);
    expect(a).to.be.equal(s.getArg());
  });
});
