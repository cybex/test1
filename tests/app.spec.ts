import assert from 'assert';
import async from 'async';
import supertest from 'supertest';
import app from '../src/app';

// test unit /a route
describe('add operator test', () => {
  // test 2 postive number for /a route
  it('Simple get for testing', (done) => {
    supertest(app)
      .get('/')
      .expect(201)
      .then(() => {
        done();
      }).catch((e) => {
        done(e);
      });
  });

  // testing defualt value route /a
  it('default value testing in route /a', (done) => {
    const numbers: any = {
      numbers: {},
    };
    supertest(app)
      .post('/a')
      .expect(200)
      .type('json')
      .send(numbers)
      .end((err, response) => {
        assert.equal(response.body, 0);
        console.log(response.body);
        done();
      });
  });

  // testing two possitive number route /a
  it('add two positive number', (done) => {
    const funs = [];
    let i: number = 0;
    while (i < 10) {
      i += 1;
      const numA: number = Math.floor(Math.random() * 100);
      const numB: number = Math.floor(Math.random() * +100);
      const result: number = numA + numB;
      const numbers: any = {
        numbers: {
          first: numA,
          second: numB,
        },
      };
      funs.push((callback: any) => {
        supertest(app)
          .post('/a')
          .type('json')
          .send(JSON.stringify(numbers))
          .expect(200)
          .end((err, response) => {
            if (err) {
              callback(err);
            } else {
              assert.equal(response.body, result);
              callback(null);
            }
          });
      });
    }
    async.parallel(funs, (e) => {
      done(e);
    });
  });
});

// 11 char err for route /a
it('over 11 positive number', (done) => {
  const numA: number = 10000000000;
  const numB: number = Math.floor(Math.random() + 1);
  const result: number = numA + numB;
  const numbers: any = {
    numbers: {
      first: numA,
      second: numB,
    },
  };
  supertest(app)
    .post('/a')
    .type('json')
    .send(numbers)
    .expect(413)
    .end(() => {
      done();
    });
});

// add a positive number with a negative number
it('positive & negative', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * 100);
    const numB: number = Math.floor(Math.random() * -100);
    const result: number = numA + numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/a')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// add two negative number
it('positive & negative', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * -100);
    const numB: number = Math.floor(Math.random() * -100);
    const result: number = numA + numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/a')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// testing defualt value route /b
it('default value testing in route /b', (done) => {
  const numbers: any = {
    numbers: {},
  };
  supertest(app)
    .post('/b')
    .expect(200)
    .type('json')
    .send(numbers)
    .end((err, response) => {
      assert.equal(response.body, 0);
      console.log(response.body);
      done();
    });
});

// testing two possitive number route /b
it('add two positive number', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * 100);
    const numB: number = Math.floor(Math.random() * 100);
    const result: number = numA * numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/b')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// 11 char err for route /b
it('over 11 positive number', (done) => {
  const numA: number = 10000000000;
  const numB: number = Math.floor(Math.random() + 1);
  const result: number = numA * numB;
  const numbers: any = {
    numbers: {
      first: numA,
      second: numB,
    },
  };
  supertest(app)
    .post('/b')
    .type('json')
    .send(numbers)
    .expect(413)
    .end(() => {
      done();
    });
});

// Multiplication a positive number with a negative number
it('positive & negative Multiplication', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * 100);
    const numB: number = Math.floor(Math.random() * -100);
    const result: number = numA * numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/b')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// Multiplication two negative number
it(' 2negative Multiplication', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * -100);
    const numB: number = Math.floor(Math.random() * -100);
    const result: number = numA * numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/b')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// testing defualt value route /c
it('default value testing in route /c', (done) => {
  const numbers: any = {
    numbers: {},
  };
  supertest(app)
    .post('/c')
    .expect(200)
    .type('json')
    .send(numbers)
    .end((err, response) => {
      assert.equal(response.body, 0);
      console.log(response.body);
      done();
    });
});

// testing two possitive number route /c
it('Subtraction two positive number', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * 100);
    const numB: number = Math.floor(Math.random() * 100);
    const result: number = numA - numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/c')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// 11 char err for route /c
it('over 11 positive number', (done) => {
  const numA: number = 10000000000;
  const numB: number = Math.random();
  const result: number = numA - numB;
  const numbers: any = {
    numbers: {
      first: numA,
      second: numB,
    },
  };
  supertest(app)
    .post('/c')
    .type('json')
    .send(numbers)
    .expect(413)
    .end(() => {
      done();
    });
});

// Subtraction a positive number with a negative number
it('positive & negative Subtraction', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * 100);
    const numB: number = Math.floor(Math.random() * -100);
    const result: number = numA - numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/c')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// Subtraction two negative number
it(' 2negative Subtraction', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * -100);
    const numB: number = Math.floor(Math.random() * -100);
    const result: number = numA - numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/c')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// testing defualt value route /d
it('default value testing in route /d', (done) => {
  const numbers: any = {
    numbers: {},
  };
  supertest(app)
    .post('/d')
    .expect(200)
    .type('json')
    .send(numbers)
    .end((err, response) => {
      assert.equal(response.body, 1);
      console.log(response.body);
      done();
    });
});

// testing two possitive number route /d
it('Division two positive number', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * 100);
    const numB: number = Math.floor(Math.random() * 100);
    const result: number = numA / numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/d')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});
it('over 11 positive number', (done) => {
  const numA: number = Math.floor(Math.random() * 100);
  const numB: number = 0;
  const numbers: any = {
    numbers: {
      first: numA,
      second: numB,
    },
  };
  supertest(app)
    .post('/d')
    .type('json')
    .send(numbers)
    .expect(400)
    .end(() => {
      done();
    });
});

// 11 char err for route /d
it('over 11 positive number', (done) => {
  const numA: number = 20000000000;
  const numB: number = Math.floor(Math.random() + 1);
  const result: number = numA / numB;
  const numbers: any = {
    numbers: {
      first: numA,
      second: numB,
    },
  };
  supertest(app)
    .post('/d')
    .type('json')
    .send(numbers)
    .expect(413)
    .end(() => {
      done();
    });
});

// Division a positive number with a negative number
it('positive & negative Division', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * 100);
    const numB: number = Math.floor(Math.random() * -100);
    const result: number = numA / numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/d')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});

// Division two negative number
it(' 2negative Division', (done) => {
  const funs = [];
  let i: number = 0;
  while (i < 10) {
    i += 1;
    const numA: number = Math.floor(Math.random() * -100);
    const numB: number = Math.floor(Math.random() * -10);
    const result: number = numA / numB;
    const numbers: any = {
      numbers: {
        first: numA,
        second: numB,
      },
    };
    funs.push((callback: any) => {
      supertest(app)
        .post('/d')
        .type('json')
        .send(JSON.stringify(numbers))
        .expect(200)
        .end((err, response) => {
          if (err) {
            callback(err);
          } else {
            assert.equal(response.body, result);
            callback(null);
          }
        });
    });
  }
  async.parallel(funs, (e) => {
    done(e);
  });
});
